package tam.workspace;

import djf.AppTemplate;
import djf.controller.AppFileController;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.file.TimeSlot;
import tam.jtps.JTPS_AddTA;
import tam.jtps.JTPS_ChangeHours;
import tam.jtps.JTPS_ToggleTA;
import tam.jtps.JTPS_Update;
import tam.jtps.jTPS;
import tam.workspace.TAWorkspace;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        } else if (email.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name) || data.containsTAEmail(email)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            String emailPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
            Pattern emailSpliter = Pattern.compile(emailPattern);
            Matcher matcher = emailSpliter.matcher(email);
            if(matcher.matches()){
//                data.addTA(name,email);
                jTPS jtps = workspace.getJTPS();
                jtps.addTransaction(new JTPS_AddTA(name,email,data));
                // CLEAR THE TEXT FIELDS
                nameTextField.setText("");
                emailTextField.setText("");
                // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
                nameTextField.requestFocus();
                emailTextField.requestFocus();
                app.getGUI().updateToolbarControls(false);
                app.getGUI().getFileController().markFileAsNotSaved();
                
            } else {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));                                    
            }
        }
    }
    
    public void handleHours(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        if(workspace.getStartBox().getSelectionModel().getSelectedIndex() > workspace.getEndBox().getSelectionModel().getSelectedIndex() && workspace.getEndBox().getSelectionModel().getSelectedIndex() < workspace.getStartBox().getSelectionModel().getSelectedIndex()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(START_HIGHER_END_TITLE), props.getProperty(START_HIGHER_END_MESSAGE));
        }else {
        String startBox = (String) workspace.getStartBox().getSelectionModel().getSelectedItem();
        String endBox = (String) workspace.getEndBox().getSelectionModel().getSelectedItem();
        
        
        String startHour = convertHour(startBox);
        String endHour = convertHour(endBox);
        
        int endHourInt = Integer.parseInt(endHour);
        int startHourInt = Integer.parseInt(startHour);
        
        
        
        
        jTPS jtps = workspace.getJTPS();
        jtps.addTransaction(new JTPS_ChangeHours(startHour,endHour,startHourInt,endHourInt,data.getOfficeHours(),data, workspace));
        }
//        if(startHourInt < endHourInt){
//            ArrayList<TimeSlot> hourList = TimeSlot.buildOfficeHoursList(data);
//            
//            
//            
//            
//            data.setStartHour(startHourInt);
//            data.setEndHour(endHourInt);
//            workspace.resetWorkspace();
//            data.initHours(startHour, endHour);
//            
//            workspace.reloadWorkspace(data);
//            
//            
//            for(TimeSlot ts : hourList){    
//                    data.addOfficeHoursReservation(ts.getDay(), ts.getTime(), ts.getName());
//            }  
//        }
    }
    
    public String convertHour(String hour){
        String convertedHour = "";
        if(hour.contains("pm")){
            String parsedHour = hour.substring(0,hour.indexOf(":"));
            int currentTime = Integer.parseInt(parsedHour);
            convertedHour = (currentTime+12+"");
        }else if(hour.contains("am")){
            convertedHour = hour.substring(0, hour.indexOf(":"));
        }
        return convertedHour;
    }
    
    public void handleClear(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        workspace.getNameTextField().clear();
        workspace.getNameTextField().requestFocus();
        workspace.getEmailTextField().clear();
        
        taTable.getSelectionModel().clearSelection();
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
        // GET THE TA
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        TAData data = (TAData)app.getDataComponent();
        String cellKey = pane.getId();
      
        app.getGUI().updateToolbarControls(false);
        app.getGUI().getFileController().markFileAsNotSaved();
        // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
        String[] splitKey = cellKey.split("_");
        String dateCol = data.getCellKey(Integer.parseInt(splitKey[0]), 0);
        String timeRow = data.getCellKey(0, Integer.parseInt(splitKey[1]));
        
        jTPS jtps = workspace.getJTPS();
        
        StringProperty day = data.getOfficeHours().get(dateCol);
        String dayValue = day.getValue();
        StringProperty time = data.getOfficeHours().get(timeRow);
        String timeValue = time.getValue();
        String scoredTime = timeValue.replace(":","_");
        
//        data.toggleTAOfficeHours(cellKey, taName);
        jtps.addTransaction(new JTPS_ToggleTA(taName,dayValue,scoredTime,data));

        }
    }
    
    public void handDeleteKey(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        TAData data = (TAData)app.getDataComponent();
        if(selectedItem != null){
        data.removeTAOfficeHours(taName);
        app.getGUI().updateToolbarControls(false);
        app.getGUI().getFileController().markFileAsNotSaved();
        }
    }
    
    public void handleUpdateTA(String oldName, String oldEmail){
        
        
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        TeachingAssistant selectedTA = (TeachingAssistant) taTable.getSelectionModel().getSelectedItem();
        TAData data = (TAData)app.getDataComponent();
        TeachingAssistant ta = data.getTA(selectedTA.getName());
        String emailPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        Pattern emailSpliter = Pattern.compile(emailPattern);
        Matcher matcher = emailSpliter.matcher(workspace.getEmailTextField().getText());
        if(matcher.matches()){
            jTPS jtps = workspace.getJTPS();
            jtps.addTransaction(new JTPS_Update(ta,data,oldName,workspace.getNameTextField().getText(),workspace.getEmailTextField().getText(),oldEmail,workspace));
            
            
            app.getGUI().updateToolbarControls(false);
            app.getGUI().getFileController().markFileAsNotSaved();
        } else {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));                                    
            
        }
        
        
    }
}
