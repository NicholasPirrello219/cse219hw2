/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author Nick
 */
public class JTPS_ToggleTA implements jTPS_Transaction{
    String name;
    String date;
    String time;
    TAData data;
    public JTPS_ToggleTA(String name, String date, String time, TAData data){
        this.name = name;
        this.date = date;
        this.time = time;
        this.data = data;
    }
    
    @Override
    public void doTransaction() {
        String cellKey = data.getCellKey(date, time);
        data.toggleTAOfficeHours(cellKey, name);
    }

    @Override
    public void undoTransaction() {
        String cellKey = data.getCellKey(date, time);
        data.toggleTAOfficeHours(cellKey, name);
    }
    
}
