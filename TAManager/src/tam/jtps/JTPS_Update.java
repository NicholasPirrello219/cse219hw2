/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import java.util.ArrayList;
import javafx.beans.property.StringProperty;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;

/**
 *
 * @author Nick
 */
public class JTPS_Update implements jTPS_Transaction{
    TAData data;
    String oldName,newName,newEmail,oldEmail;
    TAWorkspace workspace;
    TeachingAssistant ta;
    ArrayList<String> cellkeys;
    
    public JTPS_Update(TeachingAssistant ta,TAData data, String oldName, String newName, String newEmail, String oldEmail,TAWorkspace workspace){
        this.data = data;
        this.oldEmail = oldEmail;
        this.newEmail = newEmail;
        this.oldName = oldName;
        this.newName = newName;
        this.workspace = workspace;
        this.ta = ta;
        cellkeys = new ArrayList<>();
    }
    @Override
    public void doTransaction() {
        ta.setName(workspace.getNameTextField().getText());
        ta.setEmail(workspace.getEmailTextField().getText());
        workspace.getTATable().refresh();
//            taTable.refresh();
//            workspace.reloadWorkspace(data);
//            handleClear();
            //for each of the gridtapanes, if contains name, remove from col,row then toggle new one
           for(String cellKey : data.getOfficeHours().keySet()){
               StringProperty cellProp = data.getOfficeHours().get(cellKey);
               String cellText = cellProp.getValue();
               if(cellText.contains(oldName)){
                   cellkeys.add(cellKey);
                   data.removeTAFromCell(cellProp,oldName);
                   data.toggleTAOfficeHours(cellKey, workspace.getNameTextField().getText());
               }
           }
    }

    @Override
    public void undoTransaction() {
        ta.setName(oldName);
        ta.setEmail(oldEmail);
        workspace.getTATable().refresh();
        for(int i = 0;i < cellkeys.size();i++){
            data.toggleTAOfficeHours(cellkeys.get(i), oldName);
        }
    }
    
}
