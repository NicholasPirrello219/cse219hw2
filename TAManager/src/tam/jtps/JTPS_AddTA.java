/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import tam.data.TAData;

/**
 *
 * @author Nick
 */
public class JTPS_AddTA implements jTPS_Transaction{
    String name;
    String email;
    TAData data;
    public JTPS_AddTA(String name, String email, TAData data){
        this.name = name;
        this.email = email;
        this.data = data;
    }
    @Override
    public void doTransaction() {
        System.out.println("IS THIS BEING DONE?");
        data.addTA(name, email);
    }

    @Override
    public void undoTransaction() {
        data.removeTAOfficeHours(name);
    }
    
}
