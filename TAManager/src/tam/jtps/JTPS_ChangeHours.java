/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import properties_manager.PropertiesManager;
import tam.TAManagerProp;
import tam.data.TAData;
import tam.file.TimeSlot;
import tam.workspace.TAWorkspace;

/**
 *
 * @author Nick
 */
public class JTPS_ChangeHours implements jTPS_Transaction{
    String startHour;
    String endHour;
    int startHourInt;
    int endHourInt;
    int prevEndHour;
    int prevStartHour;
    HashMap<String,StringProperty> officeHours;
    TAData data;
    TAWorkspace workspace;
    public JTPS_ChangeHours(String startHour, String endHour, int startHourInt, int endHourInt, HashMap<String,StringProperty> officeHours,TAData data, TAWorkspace workspace){
        this.startHour = startHour;
        this.endHour = endHour;
        this.startHourInt = startHourInt;
        this.endHourInt = endHourInt;
        
        this.data = data;
        this.officeHours = officeHours;
        this.workspace = workspace;
    }
    @Override
    public void doTransaction() {
        if(startHourInt < endHourInt){
            int time = 0;
            ArrayList<TimeSlot> hourList = TimeSlot.buildOfficeHoursList(data);
            boolean thereIs = false;
            for(TimeSlot ts : hourList){
                String[] times = ts.getTime().split("_");
                time = Integer.parseInt(times[0]);
                if(ts.getTime().contains("pm")){
                    time += 12;
                }
                if(time > prevStartHour || time < prevEndHour){
                    thereIs = true;
                }
                
            }
            
            
            System.out.println(time);
            System.out.println(startHourInt);
            System.out.println(endHourInt);
            if(thereIs){
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.show(props.getProperty(TAManagerProp.DELETE_TA_CONFIRM_TITLE), props.getProperty(TAManagerProp.DELETE_TA_CONFIRM_MESSAGE));
                String selection = yesNoDialog.getSelection();
                if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
                    prevStartHour = data.getStartHour();
                    prevEndHour = data.getEndHour();
                    System.out.println("Do EndHour:" + endHourInt);
                    System.out.println("Do StartHour:" + startHourInt);
                        System.out.println("Do prevEndHour:" + prevEndHour);
                    System.out.println("Do prevStartHour:" + prevStartHour);
                    data.setOfficeHours(officeHours);
            
                    data.setStartHour(startHourInt);
                    data.setEndHour(endHourInt);

                    workspace.resetWorkspace();
                    data.initHours(startHour, endHour);
                    workspace.reloadWorkspace(data);
                    for(TimeSlot ts : hourList){    
                        data.addOfficeHoursReservation(ts.getDay(), ts.getTime(), ts.getName());
                    } 
                }
             
            } else {
                prevStartHour = data.getStartHour();
                    prevEndHour = data.getEndHour();
                    System.out.println("Do EndHour:" + endHourInt);
                    System.out.println("Do StartHour:" + startHourInt);
                    System.out.println("Do prevEndHour:" + prevEndHour);
                    System.out.println("Do prevStartHour:" + prevStartHour);
                    data.setOfficeHours(officeHours);
            
                    data.setStartHour(startHourInt);
                    data.setEndHour(endHourInt);

                    workspace.resetWorkspace();
                    data.initHours(startHour, endHour);
                    workspace.reloadWorkspace(data);
                    for(TimeSlot ts : hourList){    
                        data.addOfficeHoursReservation(ts.getDay(), ts.getTime(), ts.getName());
                    } 
            }
            
             
       }
    }

    @Override
    public void undoTransaction() {
        if(prevStartHour < prevEndHour){
            data.setStartHour(prevStartHour);
            data.setEndHour(prevEndHour);
            data.setOfficeHours(officeHours);
            ArrayList<TimeSlot> hourList = TimeSlot.buildOfficeHoursList(data);
            workspace.resetWorkspace();
            data.initHours(prevStartHour+"", prevEndHour+"");
            workspace.reloadWorkspace(data);
            for(TimeSlot ts : hourList){    
                data.addOfficeHoursReservation(ts.getDay(), ts.getTime(), ts.getName()); 
            }
            int tempEndHour = endHourInt;
            int tempStartHour = startHourInt;
            endHourInt = prevEndHour;
            startHourInt = prevStartHour;
            prevEndHour = tempEndHour;
            prevStartHour = tempStartHour;
            System.out.println("Undo EndHour:" + endHourInt);
            System.out.println("Undo StartHour:" + startHourInt);
            System.out.println("Undo prevEndHour:" + prevEndHour);
            System.out.println("Undo prevStartHour:" + prevStartHour);
        }
    }
    
}
